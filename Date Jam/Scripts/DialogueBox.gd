extends Control
class_name DialogueBox

onready var dialogue_player= get_node("DialoguePlayer")

func start(dialogue : Dictionary, target : String):
	print("dialogue")
	dialogue_player.start(dialogue, target)
	update_content()
	$"Name Box".show()
	if $"Name Box/Name_label".text == "":
		$"Name Box".hide()
	show()

func _input(event):
	var just_pressed = event.is_pressed() and not event.is_echo()
	if Input.is_key_pressed(KEY_SPACE) and just_pressed:
		next()

func next():
	if dialogue_player.finished:
		finished()
	else:
		dialogue_player.next()
		update_content()
		
func finished():
	dialogue_player.finished = false
	hide()

func update_content():
	if $"Name Box/Name_label".text == dialogue_player.chara_name and $"Text Box/Text_label".text == dialogue_player.text:
		finished()
	$"Name Box/Name_label".text = dialogue_player.chara_name
	$"Text Box/Text_label".text = dialogue_player.text