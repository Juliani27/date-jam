extends Node

class_name DialoguePlayer

var chara_name = ""
var text = ""
var finished = false
var target = ""
var conversation : Array
var current_index : int = 0

func start(dialogue_dict : Dictionary, interact_target : String):
	target = interact_target
	conversation = dialogue_dict.values()
	current_index = 0
	update()

func next():
	current_index += 1
	if current_index >= conversation.size():
		return
	assert current_index <= conversation.size()
	update()

func update():
	if !finished:
		if target != "" and target != conversation[current_index].target:
			#print(str(str(current_index) + " - " + target + " - " + keys[current_index]))
			if current_index == conversation.size() - 1:
				finished = true
			next()
		else: 
			chara_name = conversation[current_index].name
			text = conversation[current_index].text
			if current_index == conversation.size() - 1:
				finished = true