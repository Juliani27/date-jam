extends Area2D

export (String) var playerName = ""
export (String) var sceneName = ""
export (int) var door_position_x = 0
export (int) var door_position_y = 0
export (String) var player_sprite = ""
export (String) var target_name = ""
export (String, FILE, "*.json") var dialogue_path = ""

var can_interact = false

func _on_AreaTrigger_body_entered(body):
	if body.get_name() == playerName:
		can_interact = true
		global.player_position.x = door_position_x
		global.player_position.y = door_position_y
		global.player_sprite = player_sprite
		get_tree().change_scene(str("res://Scenes/" + sceneName + ".tscn"))

func _input(event):
	var just_pressed = event.is_pressed() and not event.is_echo()
	if Input.is_key_pressed(KEY_ENTER) and can_interact and just_pressed:
		interact(target_name)

func interact(item):
	var dialogue : Dictionary = load_dialogue()
	get_tree().get_current_scene().find_node("DialogueBox").start(dialogue, item)

func load_dialogue():
	var file = File.new()
	file.open(dialogue_path, file.READ)
	#var exist = file.file_exists(path)
	
	var dialogue = parse_json(file.get_as_text())
	#print(file.get_as_text())
	#print(dialogue)
	return dialogue

func _on_AreaTrigger_body_exited(body):
	if body.get_name() == playerName:
		can_interact = false
