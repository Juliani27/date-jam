extends Node2D

var time = 0
const UP = Vector2(0,-1)
var velocity = Vector2()

func _ready():
	
	$Riani.get_node("Camera2D").current = false
	$Ren.get_node("Camera2D").current = false
	$Leo.get_node("Camera2D").current = false
	play_animation($Ren, "Idle Right")
	play_animation($Leo, "Idle Left")

func _physics_process(delta):
	time += delta
	play()
	if time > 2 and time < 3:
		move_camera(get_player_grid_pos($Leo))

func play():
	pass

func play_animation(chara, animation):
	chara.get_node("AnimatedSprite").play(animation)
	
func move_chara(chara, x, y):
	chara.move_and_slide(Vector2(x, y), UP)

func get_player_grid_pos(player):
	var pos = player.get_position()
	return Vector2(pos.x, -pos.y + 150)
	
func move_camera(new_pos):
	$Tween.interpolate_property(self, "position", get_position(), new_pos, 1.0, Tween.TRANS_LINEAR, Tween.EASE_OUT)	
	$Tween.start()