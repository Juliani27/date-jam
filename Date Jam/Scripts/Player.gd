extends KinematicBody2D

export (int) var speed = 250

const UP = Vector2(0,-1)
var velocity = Vector2()
var freeze = false

func get_input():
	velocity.x = 0
	velocity.y = 0
	if Input.is_action_pressed('ui_up'):
		velocity.y -= speed
		$AnimatedSprite.play("Up")
		$CollisionLeft.set_process(false)
		$CollisionDefault.set_process(true)
		$CollisionRight.set_process(false)
	elif Input.is_action_pressed('ui_down'):
		velocity.y += speed
		$AnimatedSprite.play("Down")
		$CollisionLeft.set_process(false)
		$CollisionDefault.set_process(true)
		$CollisionRight.set_process(false)
	elif Input.is_action_pressed('ui_right'):
		velocity.x += speed
		$AnimatedSprite.play("Right")
		$CollisionLeft.set_process(false)
		$CollisionDefault.set_process(false)
		$CollisionRight.set_process(true)
	elif Input.is_action_pressed('ui_left'):
		velocity.x -= speed
		$AnimatedSprite.play("Left")
		$CollisionLeft.set_process(true)
		$CollisionDefault.set_process(false)
		$CollisionRight.set_process(false)
	else:
		if Input.is_action_just_released("ui_up"):
			$AnimatedSprite.play("Idle Up")
		elif Input.is_action_just_released("ui_down"):
			$AnimatedSprite.play("Idle Down")
		elif Input.is_action_just_released("ui_right"):
			$AnimatedSprite.play("Idle Right")
		elif Input.is_action_just_released("ui_left"):
			$AnimatedSprite.play("Idle Left")

func _physics_process(delta):
	if !freeze:
		get_input()
		velocity = move_and_slide(velocity, UP)

