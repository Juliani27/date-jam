extends Node2D

func _ready():
	if global.player_position.x == 0 and global.player_position.y == 0:
		global.player_position.x = 449
		global.player_position.y = 258
	$Ren.set_position(global.player_position)
	play_animation($Ren, global.player_sprite)
	$CanvasLayer/DialogueBox.hide()

func _process(delta):
	if ($CanvasLayer/DialogueBox.is_visible()):
		$Ren.freeze = true
		play_animation($Ren, str("Idle " + $Ren/AnimatedSprite.animation.split(" ")[-1]))
	if (!$CanvasLayer/DialogueBox.is_visible()):
		$Ren.freeze = false

func play_animation(chara, animation):
	chara.get_node("AnimatedSprite").play(animation)