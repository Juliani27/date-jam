extends Node2D

func _ready():
	if global.player_position.x == 0 and global.player_position.y == 0:
		global.player_position.x = 373
		global.player_position.y = 300
	$Ren.set_position(global.player_position)
	play_animation($Ren, global.player_sprite)
	$Ren.get_node("Camera2D").current = true
	$Rika.get_node("Camera2D").current = false
	play_animation($Rika, "Idle Down")
	play_animation($Riani, "Idle Up")
	$CanvasLayer/DialogueBox.hide()

func _process(delta):
	if ($CanvasLayer/DialogueBox.is_visible()):
		$Ren.freeze = true
		play_animation($Ren, str("Idle " + $Ren/AnimatedSprite.animation.split(" ")[-1]))
	if (!$CanvasLayer/DialogueBox.is_visible()):
		$Ren.freeze = false

func play_animation(chara, animation):
	chara.get_node("AnimatedSprite").play(animation)
